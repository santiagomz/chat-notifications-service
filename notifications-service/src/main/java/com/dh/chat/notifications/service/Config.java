package com.dh.chat.notifications.service;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Santiago Mamani
 */
@Configuration
@EnableFeignClients
@ComponentScan("com.dh.chat.notifications.service")
public class Config {

}
